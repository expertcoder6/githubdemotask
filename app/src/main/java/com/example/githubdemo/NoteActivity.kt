package com.example.githubdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.Person.fromBundle
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.githubdemo.persistence.Note
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class NoteActivity : AppCompatActivity() {

    lateinit var noteViewModel: NoteViewModel

    @Inject
    lateinit var viewmodelProviderFactory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)

        val mbtn_save: Button = findViewById(R.id.btn_save)
        val bundle = intent.extras
        var userName: String? = null
        userName = bundle!!.getString("user_login")
        val imgUser: ImageView = findViewById(R.id.img_user)
        val txt_followers: TextView = findViewById(R.id.txt_followers)
        val txt_following: TextView = findViewById(R.id.txt_following)
        val txt_username: TextView = findViewById(R.id.txt_username)
        val txt_company: TextView = findViewById(R.id.txt_company)
        val txt_blog: TextView = findViewById(R.id.txt_blog)
        val toolbarTitle: TextView = findViewById(R.id.toolbarTitle)
        ApiInterface.create().getUserDetails(userName).enqueue(object : Callback<GetUserList> {
            override fun onResponse(call: Call<GetUserList>, response: Response<GetUserList>) {
                Log.i("TAG", "25 post submitted to API. Note Activity" + response)
                Log.i("TAG", "25 post submitted to API." + response.body())
                if (response.isSuccessful()) {
                    Log.i("", "32 post registration to API " + response.body()!!.toString())
                    Glide.with(applicationContext).load(response.body()!!.avatar_url)
                        .apply(RequestOptions().centerCrop())
                        .into(imgUser)
                    txt_company.setText("Company : " + response.body()!!.company)
                    txt_username.setText("Name : " + response.body()!!.name)
                    txt_blog.setText("Blog : " + response.body()!!.blog)
                    txt_followers.setText("Follower : " + response.body()!!.followers.toString())
                    txt_following.setText("Following : " + response.body()!!.following.toString())
                    toolbarTitle.setText(response.body()!!.login)

                }
            }

            override fun onFailure(call: Call<GetUserList>, t: Throwable) {

            }
        })


        mbtn_save.setOnClickListener {
            //    saveNote()
        }


    }


}