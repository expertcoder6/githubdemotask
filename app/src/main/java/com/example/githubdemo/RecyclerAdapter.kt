package com.example.githubdemo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.callbackFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.coroutines.withContext as withContext1

class RecyclerAdapter(var dataList: List<UserList>, val context: Context) :
    RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {

    var userList: List<UserList> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.github_list_row, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        //return userList == null ? 0 : userList.size()
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.tvMovieName.text = userList.get(position).login
        Glide.with(context).load(userList.get(position).avatar_url)
            .apply(RequestOptions().centerCrop())
            .into(holder.image)

        holder.noteIcon.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putString("user_login", userList.get(position).login)
            var intent = Intent(context, NoteActivity::class.java)
            intent.putExtras(bundle)
            startActivity(context, intent, bundle)
        })

        holder.itemView.setOnClickListener(View.OnClickListener { view: View ->
            val bundle = Bundle()
            bundle.putString("user_login", userList.get(position).login)
            var intent = Intent(context, ProfileActivity::class.java)
            intent.putExtras(bundle)
            startActivity(context, intent, bundle)


            //val apiInterface = ApiInterface.create().registrationPost(userList.get(position).login)


        })
    }

    fun setUserListItems(userList: List<UserList>) {
        this.userList = userList;
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        val tvMovieName: TextView = itemView!!.findViewById(R.id.txt_user)
        val image: ImageView = itemView!!.findViewById(R.id.img_user)
        val noteIcon: ImageView = itemView!!.findViewById(R.id.img_note)
    }

    fun filterList(filteredNames: ArrayList<UserList>) {
        // this.dataList.clear()
        this.dataList = filteredNames
        notifyDataSetChanged()
    }



}
