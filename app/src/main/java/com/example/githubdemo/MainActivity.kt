package com.example.githubdemo


import android.app.ProgressDialog
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.contains as contains

class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    lateinit var recyclerAdapter: RecyclerAdapter
    lateinit var progerssProgressDialog: ProgressDialog
    var dataList = ArrayList<UserList>()
    var filterData = ArrayList<UserList>()

    var mUtils = Utils()
    private var isLoading: Boolean = false
    lateinit var layoutManager: LinearLayoutManager
    lateinit var mEditSearch: EditText
    lateinit var mnetConnection: TextView
//    lateinit var mSearchCard: CardView
    lateinit var mSearchView: androidx.appcompat.widget.SearchView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        recyclerView = findViewById(R.id.recyclerview)
        mSearchView = findViewById(R.id.userlist_search)
        layoutManager = LinearLayoutManager(this)
        recyclerAdapter = RecyclerAdapter(dataList, this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recyclerAdapter
        loadMore()
        mnetConnection = findViewById(R.id.txt_netconnection)
        progerssProgressDialog = ProgressDialog(this)
        progerssProgressDialog.setTitle("Loading")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()
        if (mUtils.isNetworkAvailable(context = applicationContext)) {
            addScrollerListener()
        } else {
            progerssProgressDialog.dismiss()
            mnetConnection.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
            mSearchView.visibility = View.GONE
            //Toast.makeText(applicationContext,"Internet connection is not available",Toast.LENGTH_SHORT).show()
        }



        mSearchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.e("TAG", " onQueryTextSubmit: " + query )
                mSearchView.clearFocus()

                for (i in dataList.indices ){
                    Log.e("TAG", "77 onQueryTextSubmit:i " + i )
                   if (dataList[i].login == query){
                       filterData.add(dataList[i])
                       recyclerAdapter.setUserListItems(filterData)
                   }
                }
               return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                recyclerAdapter.setUserListItems(dataList)
                return false
            }

        })

    }



    private fun addScrollerListener() {
        //attaches scrollListener with RecyclerView
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!isLoading) {
                    //findLastCompletelyVisibleItemPostition() returns position of last fully visible view.
                    ////It checks, fully visible view is the last one.
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == dataList.size - 1) {
                        loadMore()
                        isLoading = true
                    }
                }
            }
        })
    }

    private fun loadMore() {
        val apiInterface = ApiInterface.create().getUser()
        apiInterface.enqueue(object : Callback<List<UserList>> {
            override fun onResponse(call: Call<List<UserList>>?,response: Response<List<UserList>>?) {
                progerssProgressDialog.dismiss()
                mnetConnection.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                mSearchView.visibility = View.VISIBLE
                if (response?.body() != null) {
                    dataList.addAll(response?.body()!!)
                    recyclerAdapter.setUserListItems(response?.body()!!)
                } else {
                    Toast.makeText(applicationContext, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<UserList>>?, t: Throwable?) {
                progerssProgressDialog.dismiss()
            }
        })
    }
}