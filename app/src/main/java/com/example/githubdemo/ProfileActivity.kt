package com.example.githubdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val bundle = intent.extras
        var userName:String? = null
        userName = bundle!!.getString("user_login")

        val tvUserName: TextView = findViewById(R.id.txt_username)
        val tvCompany: TextView = findViewById(R.id.txt_company)
        val tvNodeId: TextView = findViewById(R.id.txt_node_id)
        val txt_id: TextView = findViewById(R.id.txt_id)
        val txt_gravatar_id: TextView = findViewById(R.id.txt_gravatar_id)
        val txt_url: TextView = findViewById(R.id.txt_url)
        val txt_html_url: TextView = findViewById(R.id.txt_html_url)
        val txt_followers: TextView = findViewById(R.id.txt_followers)
        val txt_following: TextView = findViewById(R.id.txt_following)
        val txt_gists_url: TextView = findViewById(R.id.txt_gists_url)
        val txt_starred_url: TextView = findViewById(R.id.txt_starred_url)
        val txt_subscriptions_url: TextView = findViewById(R.id.txt_subscriptions_url)
        val txt_organizations_url: TextView = findViewById(R.id.txt_organizations_url)
        val txt_repos_url: TextView = findViewById(R.id.txt_repos_url)
        val txt_events_url: TextView = findViewById(R.id.txt_events_url)
        val txt_received_events_url: TextView = findViewById(R.id.txt_received_events_url)
        val txt_type: TextView = findViewById(R.id.txt_type)
        val txt_site_admin: TextView = findViewById(R.id.txt_site_admin)
        val txt_name: TextView = findViewById(R.id.txt_name)
        val txt_blog: TextView = findViewById(R.id.txt_blog)
        val txt_location: TextView = findViewById(R.id.txt_location)
        val txt_email: TextView = findViewById(R.id.txt_email)
        val txt_hireable: TextView = findViewById(R.id.txt_hireable)
        val txt_twitter_username: TextView = findViewById(R.id.txt_twitter_username)
        val txt_public_repos: TextView = findViewById(R.id.txt_public_repos)
        val txt_public_gists: TextView = findViewById(R.id.txt_public_gists)
        //val txt_followers_url: TextView = findViewById(R.id.txt_followers_url)
        val txt_following_url: TextView = findViewById(R.id.txt_following_url)
        val txt_created_at: TextView = findViewById(R.id.txt_created_at)
        val txt_updated_at: TextView = findViewById(R.id.txt_updated_at)
        val txt_id_count: TextView = findViewById(R.id.txt_id_count)
        val txt_followers_count: TextView = findViewById(R.id.txt_followers_count)
        val txt_following_count: TextView = findViewById(R.id.txt_following_count)
        val imgUser: ImageView = findViewById(R.id.img_user)
        tvUserName.setText("Name: " + userName)

        Log.e("TAG", "22 onCreate:bundle " + bundle!!.getString("user_login"))
        ApiInterface.create().getUserDetails(userName).enqueue(object : Callback<GetUserList> {
                override fun onResponse(call: Call<GetUserList>, response: Response<GetUserList>) {
                    Log.i("TAG", "25 post submitted to API." + response)
                    Log.i("TAG", "25 post submitted to API." + response.body())
                    if (response.isSuccessful()) {
                        Log.i("", "32 post registration to API " + response.body()!!.toString())
                        Glide.with(applicationContext).load(response.body()!!.avatar_url)
                            .apply(RequestOptions().centerCrop())
                            .into(imgUser)
                        tvCompany.setText("Company : " + response.body()!!.company)
                        tvNodeId.setText("Node Id : " + response.body()!!.node_id)
                        txt_gravatar_id.setText("Gravatar Id : " + response.body()!!.gravatar_id)
                        txt_url.setText("Url : " + response.body()!!.url)
                        txt_html_url.setText("Html url : "+response.body()!!.html_url)
                        txt_followers_count.setText( response.body()!!.followers.toString())
                        txt_following_count.setText( response.body()!!.following.toString())
                        //txt_followers_url.setText("Followers url : " + response.body()!!.followers_url)
                        txt_following_url.setText("Following url : " + response.body()!!.following_url)
                        txt_gists_url.setText("Gists_url : " + response.body()!!.gists_url)
                        txt_starred_url.setText("starred_url : " + response.body()!!.starred_url)
                        txt_subscriptions_url.setText("Subscriptions url : " + response.body()!!.subscriptions_url)
                        txt_organizations_url.setText("Organizations url : " + response.body()!!.organizations_url)
                        txt_repos_url.setText("Repos url : " + response.body()!!.repos_url)
                        txt_events_url.setText("Events url : " + response.body()!!.events_url)
                        txt_received_events_url.setText("Received events url : " + response.body()!!.received_events_url)
                        txt_type.setText("type : " + response.body()!!.type)
                        txt_site_admin.setText("Site Admin : " + response.body()!!.site_admin.toString())
                        txt_name.setText("Name : " + response.body()!!.name)
                        txt_blog.setText("Blog : " + response.body()!!.blog)
                        txt_location.setText("Location : " + response.body()!!.location)
                        txt_email.setText("Email : " + response.body()!!.email)
                        txt_hireable.setText("Hireable : " + response.body()!!.hireable)
                        txt_twitter_username.setText("Twitter Name : " +  response.body()!!.twitter_username)
                        txt_public_repos.setText("Public repos : " + response.body()!!.public_repos.toString())
                        txt_public_gists.setText("Public gists : " + response.body()!!.public_gists.toString())
                        txt_created_at.setText("Created at : " + response.body()!!.created_at)
                        txt_updated_at.setText("Updated at : " + response.body()!!.updated_at)
                        txt_id_count.setText( response.body()!!.id.toString())
                    }
                }

                override fun onFailure(call: Call<GetUserList>, t: Throwable) {

                }
            })
    }
}