package com.example.githubdemo

import android.util.Log
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @GET("users?since=0")
    fun getUser() : Call<List<UserList>>

    @GET("users/{username}")
    fun getUserDetails(@Path("username") username: String? ): Call<GetUserList>;


    companion object {
        var BASE_URL = "https://api.github.com/"
        fun create() : ApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            Log.e("TAG", "42 create:BASE_URL " + BASE_URL )
            return retrofit.create(ApiInterface::class.java)

        }
    }
}